# SemGrep tool

https://semgrep.dev/

Semgrep is a lightweight, offline, open-source, static analysis tool.

## Uee it

```yaml
stages:
  - 🔎secure

include:
  - project: 'simplify-devops/semgrep'
    file: 'semgrep.gitlab-ci.yml'

#-----------------------------------------------------------------------------------------
# Security
#-----------------------------------------------------------------------------------------
👮‍♀️:vulnerability:detection:
  stage: 🔎secure
  extends: .semgrep:analyzer  
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID

```
